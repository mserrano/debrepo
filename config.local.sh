#!/bin/sh
#*=====================================================================*/
#*    serrano/prgm/utils/debrepo/config.local.sh                       */
#*    -------------------------------------------------------------    */
#*    Author      :  Manuel Serrano                                    */
#*    Creation    :  Thu Mar 26 09:20:50 2020                          */
#*    Last change :  Mon Apr  5 08:34:51 2021 (serrano)                */
#*    Copyright   :  2020-21 Manuel Serrano                            */
#*    -------------------------------------------------------------    */
#*    Configuration for local tarballs files                           */
#*=====================================================================*/

REPO=$HOME/Downloads
