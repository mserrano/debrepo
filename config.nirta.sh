#!/bin/sh
#*=====================================================================*/
#*    serrano/prgm/utils/debrepo/config.nirta.sh                       */
#*    -------------------------------------------------------------    */
#*    Author      :  Manuel Serrano                                    */
#*    Creation    :  Thu Mar 26 09:20:50 2020                          */
#*    Last change :  Thu Mar 26 09:28:05 2020 (serrano)                */
#*    Copyright   :  2020 Manuel Serrano                               */
#*    -------------------------------------------------------------    */
#*    Nirta (2020 Ubuntu laptop) specific configuration.               */
#*=====================================================================*/

. ./config.local.sh
