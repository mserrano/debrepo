#!/bin/sh
#*=====================================================================*/
#*    serrano/prgm/utils/debrepo/config.sh                             */
#*    -------------------------------------------------------------    */
#*    Author      :  Manuel Serrano                                    */
#*    Creation    :  Wed Mar 25 09:03:17 2020                          */
#*    Last change :  Tue Apr 20 08:18:35 2021 (serrano)                */
#*    Copyright   :  2020-21 Manuel Serrano                            */
#*    -------------------------------------------------------------    */
#*    configuration variables for makedebrepo.sh                       */
#*=====================================================================*/

# The list of projects to be included in the repository
# This list the directories that each contain a "makedeb.sh" script
# that build its .deb files
REPO=$HOME/prgm/distrib
PACKAGES="@REPO@/bigloo-4.4c.tar.gz @REPO@/hop-3.5.0-pre1.tar.gz @REPO@/hopdac-1.2.1.hz @REPO@/hiphop-1.1.2.tar.gz"

# Prefix (use to adjust PATH before compiling)
PREFIX=/opt/hop

# The name of the distribution
COMPONENT=hop

# The directory where are stored the tarball files
REPODIR=/tmp/REPO

# The directory where to build the debian repository
DEBIANDIR=/misc/DEBIAN

# Where to upload the repo (only used --upload enabled)
UPLOAD_SERVER=hop.inria.fr
UPLOAD_DIR=linux
UPLOAD_USER=hop

HTTP_SERVER=http://hop.inria.fr
HTTP_DIR=$UPLOAD_DIR

UPLOAD_PRECMD="ssh ${UPLOAD_USER}@${UPLOAD_SERVER} sh -c \"chmod u+w -R $UPLOAD_DIR\""
UPLOAD_POSTCMD="ssh ${UPLOAD_USER}@${UPLOAD_SERVER} sh -c \"chmod a-w,a+rx -R $UPLOAD_DIR\""

# default answer to "yes" or "no" questions, such as "starts hop on reboot?"
YESORNO=no

# per host configuration
if [ -f ./config.`hostname`.sh ]; then
  . ./config.`hostname`.sh
fi
