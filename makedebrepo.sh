#!/bin/bash
#*=====================================================================*/
#*    serrano/prgm/utils/debrepo/makedebrepo.sh                        */
#*    -------------------------------------------------------------    */
#*    Author      :  Manuel Serrano                                    */
#*    Creation    :  Wed Mar 25 09:02:28 2020                          */
#*    Last change :  Mon Apr  5 08:50:05 2021 (serrano)                */
#*    Copyright   :  2020-21 Manuel Serrano                            */
#*    -------------------------------------------------------------    */
#*    The shell script to build a debian/ubuntu repository             */
#*=====================================================================*/

#*---------------------------------------------------------------------*/
#*    user configuration                                               */
#*---------------------------------------------------------------------*/
. ./config.sh

#*---------------------------------------------------------------------*/
#*    parameters (with optional default values)                        */
#*---------------------------------------------------------------------*/
build=
upload=no
sign=
yesorno="--yes-or-no=no"

# linux version and architecture
codename=
id=
usedocker=false

#*---------------------------------------------------------------------*/
#*    help ...                                                         */
#*---------------------------------------------------------------------*/
function help() {
  if [ "$1" != "--help" ]; then
    echo "*** makedebrepo.sh error, unknown option $1" >&2
    echo >&2
  fi
  echo "Usage: makedebrepo [options] [pkg]" >&2
  echo "" >&2
  echo "  -b|--build............ build packages (default)" >&2
  echo "  -n|--no-build......... don't build packages" >&2
  echo "  --repodir............. repo directory (target directory)" >&2
  echo "  --upload.............. upload the repo directory" >&2
  echo "  --no-sign............. do not sign packages" >&2
  echo "  -i|--id............... distribution identity" >&2
  echo "  -c|--codename......... distribution codename" >&2
  echo "  -a|--arch............. distribution architecture" >&2
  echo "  --yes-or-no=[yes|no].. answer \"yes\" or \"no\" to questions" >&2
  echo "  --docker.............. build the packages using docker" >&2
}

#*---------------------------------------------------------------------*/
#*    command line parsing                                             */
#*---------------------------------------------------------------------*/
projects=

while : ; do
  case $1 in
    "")
      break;;
    -h|--help)
      help $1
      exit 1;;
    -b|--build)
      build=;;
    -n|--no-build)
      build=no;;
    -i|--id)
      shift
      id=$1;;
    -c|--codename)
      shift
      codename=$1;;
    -a|--arch)
      shift
      arch=$1;;
    --repodir)
      shift
      REPODIR=$1;;
    --upload)
      upload=yes;;
    --no-sign)
      sign=--no-sign;;
    --docker)
      usedocker=true;;
    --yes-or-no=no)
      yesorno="--yes-or-no=no";;
    --yes-or-no=yes)
      yesorno="--yes-or-no=yes";;
    -*)
      help $1
      exit 1;;
    *)
      projects="$1 $projects";;

  esac
  shift
done

if [ "$projects " != " " ]; then
  PROJECTS=$projects
else
  for p in $PACKAGES; do
    PROJECTS=`echo $PACKAGES | sed -e "s|@REPO@|$REPO|g"`
  done
fi  

#*---------------------------------------------------------------------*/
#*    apt message                                                      */
#*---------------------------------------------------------------------*/
function summary() {
  echo ""
  echo "-----------------------------------------------------------------------"
  echo "Repo complete... \"$componentdir\""
  echo ""
  if [ "$upload " != "yes " ]; then
    echo "To upload manually (use \"--upload\" to upload automatically)"
    echo "  rsync -vpogt -r $DEBIANDIR/$id $UPLOAD_USER@$UPLOAD_SERVER:$UPLOAD_DIR"
    echo ""
  fi  
  echo "Debian $release"
  echo "  echo \"deb [trusted=yes] $HTTP_SERVER/$HTTP_DIR/$id $codename $COMPONENT\" > /etc/apt/sources.list.d/hop.list"
  echo "-----------------------------------------------------------------------"
}

#*---------------------------------------------------------------------*/
#*    init_dirs ...                                                    */
#*---------------------------------------------------------------------*/
function init_dirs() {
  componentdir=$DEBIANDIR/$id/dists/$codename/$COMPONENT
  debbindir=$componentdir/binary-$arch

  mkdir -p $debbindir
  mkdir -p $REPODIR
}

#*---------------------------------------------------------------------*/
#*    build via docker                                                 */
#*---------------------------------------------------------------------*/
if [ "$usedocker " = "true " ]; then

  mkdir -p docker/tmp/distrib
  cp makedebrepo.sh docker/tmp
  cp config.sh docker/tmp

  if [ "$id " = " " ]; then
    echo "*** ERROR: missing distribution \"id\""
    echo "for instance: $1 -i ubuntu"
    exit 1
  fi

  projs=
  for p in $projects; do
    dir=`dirname $p`
    pl=`echo $p | sed -e "s|$dir|/home/visitor/prgm/distrib|g"`
    projs="$pl $projs"
  done
    
  if [ "$codename " = " " ]; then
    cat docker/Dockerfile.in | \
      sed -e "s|@DISTRIB@|$id|" \
	  -e "s|@PROJECTS@|$projs|" > docker/Dockerfile
  else
    cat docker/Dockerfile.in | \
      sed -e "s|@DISTRIB@|$id:$codename|" \
	  -e "s|@PROJECTS@|$projs|" > docker/Dockerfile
  fi

  for p in $PROJECTS; do
    file=`basename $p`
    cp $p docker/tmp/distrib/$file
  done

  if [ "$build " != "no " ]; then
    rm -rf /tmp/debrepo_docker
    mkdir /tmp/debrepo_docker
    
    docker build -t debrepo docker \
      && name=`docker run -d --entrypoint=/bin/bash debrepo` \
      && docker cp -a $name:/misc/DEBIAN /tmp/debrepo_docker \
      && docker stop $(docker ps -a -q) \
      && docker rm $(docker ps -a -q)

    id=`ls /tmp/debrepo_docker/DEBIAN | sed 's|/||'`
    rsync -vpogt -r /tmp/debrepo_docker/DEBIAN/$id $DEBIANDIR
    rm -rf /tmp/debrepo_docker/DEBIAN/$id
    
    summary
    echo ""
    echo "Run \"docker rmi debrepo\" do remove the generated docker image."
  fi
  
  rm -rf docker/tmp
  
  exit $?
fi

#*---------------------------------------------------------------------*/
#*    local configuration                                              */
#*---------------------------------------------------------------------*/
if [ "$codename " = " " ]; then
  codename=`lsb_release -c | awk -F: '{ print $2 }' | sed -e 's/[\t ]//g'`
fi
if [ "$id " = " " ]; then
  id=`lsb_release -i | awk -F: '{ print $2 }' | sed -e 's/[\t ]//g'`
fi
if [ "$arch " = " " ]; then
  arch=`dpkg --print-architecture`
fi  

release=`lsb_release -r | awk -F: '{ print $2 }' | sed -e 's/[\t ]//g'`
description=`lsb_release -d | awk -F: '{ print $2 }' | sed -e 's/^[\t ]//g'`
distrib=`echo $id | sed 's/[A-Z]/\L&/g'`

#*---------------------------------------------------------------------*/
#*    init                                                             */
#*---------------------------------------------------------------------*/
init_dirs

#*---------------------------------------------------------------------*/
#*    makedeb_prepare ...                                              */
#*---------------------------------------------------------------------*/
function makedeb_prepare() {
  if [[ $p == "http://*" ]] || [[ $p == "https://*" ]]  || [[ $p == "ftp://*" ]]; then
    (cd $REPODIR; wget $p)

    p=$REPODIR/$p
  fi

  if [ -f $p ]; then
    case $p in
      *.tar.gz)
	pname=`basename $p .tar.gz`
	;;
      *.tgz)
	pname=`basename $p .tgz`
	;;
      *.hz)
	pname=`basename $p .hz`
	;;
      *)
	echo "*** ERROR: unsupported package name \"$p\"";
    esac
    dir=`basename $pname | sed 's/-.*$//'`
    
    rm -rf $REPODIR/$pname
    rm -rf $REPODIR/$dir

    # prepare the tarball and the source file in REPODIR
    (cd $REPODIR; cp $p .; tar xvfz `basename $p`; mv $pname $dir) || exit 1
    
    # run configure to generate the arch/debian/makedeb.sh script
    (cd $REPODIR/$dir; ./configure --prefix=$PREFIX)
    
    p=$REPODIR/$dir
  elif [ -d $p ]; then
    echo "*** ERROR: unsupported package format \"$p\"";
  else
    echo "*** ERROR: file not found \"$p\"";
  fi
}  
  
#*---------------------------------------------------------------------*/
#*    makedeb ...                                                      */
#*---------------------------------------------------------------------*/
function makedeb() {
  p=$1

  makedeb_prepare $p
  
  if [ ! -d $p ]; then
    echo "*** ERROR: not a package directory \"$p\""
    exit 1
  elif [ ! -f $p/arch/debian/makedeb.sh ]; then
    echo "*** ERROR: package directory misses \"$p/arch/debian/makedeb.sh\""
    exit 1
  fi

  (cd $p/arch/debian; ./makedeb.sh $sign $yesorno --repodir $REPODIR -O /tmp/debrepo/`basename $p`) || exit 2
}

#*---------------------------------------------------------------------*/
#*    PATH                                                             */
#*---------------------------------------------------------------------*/
PATH=$PREFIX/bin:$PATH
export PATH

#*---------------------------------------------------------------------*/
#*    optional compilation                                             */
#*---------------------------------------------------------------------*/
if [ "$build " != "no " ]; then
  for p in $PROJECTS; do
    makedeb $p
  done
fi  

#*---------------------------------------------------------------------*/
#*    copy the deb files                                               */
#*---------------------------------------------------------------------*/
# remove first and then...
for p in /tmp/debrepo/*; do
  pkg=`basename $p`
  
  rm -f $debbindir/*$pkg*_$arch.deb 2> /dev/null
  rm -f $debbindir/*$pkg*_all.deb 2> /dev/null
done

# copy, otherwise, the removal might remove already copied files
for p in /tmp/debrepo/*; do
  pkg=`basename $p`
  
  cp $p/*$pkg*_$arch.deb $debbindir 
  cp $p/*$pkg*_all.deb $debbindir 2> /dev/null
done

#*---------------------------------------------------------------------*/
#*    packages.gz                                                      */
#*---------------------------------------------------------------------*/
(cd $DEBIANDIR/$id/; dpkg-scanpackages dists/$codename/$COMPONENT/binary-$arch /dev/null | gzip -9c >  dists/$codename/$COMPONENT/binary-$arch/Packages.gz)

#*---------------------------------------------------------------------*/
#*    Release                                                          */
#*---------------------------------------------------------------------*/
# collect all the aritechture
cat > $DEBIANDIR/$id/dists/$codename/Release << EOF
Origin: $id
Label: $id
Version: $release
Codename: $codename
Date: `date -R -u`
Architectures: `(cd $componentdir; ls -d binary-* | sed 's/binary-//' | sed 's|/||')`
Components: $COMPONENT
Description: $description
EOF

(cd $DEBIANDIR/$id/dists/$codename && 
     echo "MD5Sum:" >> $DEBIANDIR/$id/dists/$codename/Release;
     for p in `find . -name Packages.gz -print`; do
       echo " `md5sum $p | awk '{print $1}'` `wc -c $p | awk '{print $1}'` $p" >> $DEBIANDIR/$id/dists/$codename/Release;
     done
     for p in `find . -name '*.deb' -print`; do
       echo " `md5sum $p | awk '{print $1}'` `wc -c $p | awk '{print $1}'` $p" >> $DEBIANDIR/$id/dists/$codename/Release;
     done
     echo "SHA256Sum:" >> $DEBIANDIR/$id/dists/$codename/Release;
     for p in `find . -name Packages.gz -print`; do
       echo " `sha256sum $p | awk '{print $1}'` `wc -c $p | awk '{print $1}'` $p" >> $DEBIANDIR/$id/dists/$codename/Release;
     done
     for p in `find . -name '*.deb' -print`; do
       echo " `sha256sum $p | awk '{print $1}'` `wc -c $p | awk '{print $1}'` $p" >> $DEBIANDIR/$id/dists/$codename/Release;
     done)

if [ "$sign " != "--no-sign " ]; then
  gpg -a -b -s $DEBIANDIR/$id/dists/$codename/Release && \
    mv $DEBIANDIR/$id/dists/$codename/Release.asc $DEBIANDIR/$id/dists/$codename/Release.gpg
fi

#*---------------------------------------------------------------------*/
#*    uploading                                                        */
#*---------------------------------------------------------------------*/
if [ "$upload " = "yes " ]; then
  if [ "$UPLOAD_PRECMD " != " " ]; then
    $UPLOAD_PRECMD
  fi

  echo "uploading .deb files..."
  echo "executing \"rsync -vpogt -r $DEBIANDIR/$id $UPLOAD_USER@$UPLOAD_SERVER:$UPLOAD_DIR\"..."
  rsync -vpogt -r $DEBIANDIR/$id $UPLOAD_USER@$UPLOAD_SERVER:$UPLOAD_DIR/

  if [ "$UPLOAD_POSTCMD " != " " ]; then
    $UPLOAD_POSTCMD
  fi
fi

#*---------------------------------------------------------------------*/
#*    done                                                             */
#*---------------------------------------------------------------------*/
summary

