Build a Hop Debian/Ubuntu Repository - 5 apr 2021
=================================================

Debrepo is a tool that builds a debian repository that can be used by
Debian's apt to install its packages.

The script runs natively on a phyisical host or it can use docker to
run the packages constructions inside a docker container. This second
option is useful to build packages on other distributions, for
instance, building the packages for Ubuntu from a Debian hosts.

The user configuration is located in `config.sh` file. The file 
`makedebrepo.sh` should _not be modified_ for user configuration.
The idea is also to let `config.sh` as unmodified as possible
and to provide additional `config.HOSTNAME.sh` files that override
some global definitions.

Basic Usage
-----------

Executed with no argument:

```shell
./makedebrepo.sh
```

The script will build a debian repository containing all the debian
packages for projects listed in `config.sh`. It will build the
packages for for the current host Debian version and
architecture. This will be stored in the directory `REPODIR` defined
in `config.sh`.

The list of packages might be overriden in `config.HOSTNAME.sh` file.


config.sh
---------

The `PACKAGES` variable of `config.sh` is a list of packages to be
installed. Each entry is either:

  1. The directory name of the package.
  2. A tarball of a project dirctory, prefixed with the string "@REPO@/".
  3. A URL, prefixed with either `http://`, `https://`, or `ftp://` of
     a package tarball.
	 
Each package _must_ have a script named `arch/debian/makedeb.sh` that builds
its debian `.deb` files.

The `REPORDIR` is the directory name where the Debian repo will be build.

The `COMPONENT` variable, is the Debian repo component name.


Advanced Usages
---------------

To upload the repo on the remote web server:

```shell
./makedebrepo.sh --upload
```

To upload the repo without rebuilding the packages:

```shell
./makedebrepo.sh -n --upload
```

To disable gpg signatures:

```shell
./makedebrepo.sh --no-sign
```

To build only one package

```shell
./makedebrepo.sh $HOME/prgm/distrib/bigloo-4.3h.tar.gz
```


Building via Docker
-------------------

When the `--docker` option is used, `makederepo` builds a
`Dockerfile`, spawns docker to build a container and then extracts the
generated packages.

```shell
./makedebrepo.sh --docker -i ubuntu -c focal 
./makedebrepo.sh --docker -i ubuntu -c bionic
./makedebrepo.sh --docker -i ubuntu
./makedebrepo.sh --docker -i ubuntu -c 18.04
./makedebrepo.sh --docker -i ubuntu -c 20.04
```

It is also possible to pass the explicit list of packages that should be
built:

```shell
./makedebrepo.sh --docker -i ubuntu -c 18.04 $HOME/prgm/distrib/bigloo-4.3h.tar.gz
```

Installing the repo
-------------------

To install the repos on hop.inria.fr:

```shell
for p in /misc/DEBIAN/*; do rsync -vpogt -r $p hop@hop.inria.fr:linux; done  
```
